Module go pour trouver le fichier le plus récent dans un répertoire.

Utilisation :
```bash
go get -u gitlab.com/go_modules/findnewestfile
```

```go
import (
    "gitlab.com/go_modules/findnewestfile"
)

func main() {
    // Find newest file in directory "my_dir"
    newestFile.Find("/my_dir")
}
```